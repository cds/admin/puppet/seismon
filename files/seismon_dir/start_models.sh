#!/bin/bash
set -e
source /var/opt/conda/base/bin/activate
conda activate seismon 
# python models.py --purge
python models.py
