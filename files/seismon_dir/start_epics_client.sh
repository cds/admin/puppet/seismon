#!/bin/bash
export PATH=$PATH:~/.local/bin
set -e
source /var/opt/conda/base/bin/activate
conda activate seismon
seismon_epics_client_db.py