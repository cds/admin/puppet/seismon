#!/bin/bash
set -e
source /var/opt/conda/base/bin/activate
conda activate seismon 
mkdir -p ProductClient
cd ProductClient
cp ../ProductClient.zip .
unzip -o ProductClient.zip
chmod 755 *.sh
cd ..
touch client_installed.flag
