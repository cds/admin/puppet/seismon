#!/bin/bash
set -e
source /var/opt/conda/base/bin/activate
conda activate seismon 
python3 setup.py install --user
touch ../src_installed.flag

