class seismon (
    String $site,
    String $ifo,
		String $conda_home,
    String $repo_url,
    String $repo_branch, # can also be a tag name
){

  $seismon_dir = '/var/lib/seismon'

  # assumes conda installed
 
$conda_defs = "${conda_home}/defs"

file {$conda_defs: ensure => directory, }

file {"${conda_defs}/seismon.yaml":
	source => 'puppet:///modules/seismon/conda/seismon.yaml',
}
#
# exec {'create_seismon_env':
#	command => "${mamba} env create -f ${condabase}/seismon.yaml -n seismon",
#	require => [File["${condabase}/seismon.yaml"], ],
#	creates => $env,
#	user => 'seismon',
#	group => 'seismon',
#	}

  ### create seismon user

  group {'seismon':
	ensure => present,
	gid => 2001,
	}

  user {'seismon':
	system => true,
	ensure => present,
	uid => 2001,
	require => Group['seismon'],
	home => $seismon_dir,
	}

  ### Install postgres
  package{['postgresql']: ensure => installed, }

  service {'postgresql':
	enable => true,
	ensure => running,
	require => Package['postgresql'],
	}

  ### allow seismon access to postgres
  file{'/etc/postgresql/13/main/pg_hba.conf':
	source => 'puppet:///modules/seismon/etc/postgresql/13/main/pg_hba.conf',
	ensure => 'present',
	notify => Service['postgresql'],
	}

  ### create database

  file {'/var/lib/postgresql/seismon_db_script':
	ensure => 'present',
	source => 'puppet:///modules/seismon/root/seismon_db_script',
	}

  exec {'create_seismon_db':
	user => postgres,
	command => '/usr/bin/psql --file=/var/lib/postgresql/seismon_db_script',
	require => File['/var/lib/postgresql/seismon_db_script'],
	unless => ['/usr/bin/psql -lqt | cut -d \| -f 1 | grep -qw seismon'], 
	}
  
# install seismon

  package {['git', 'default-jre', 'unzip', 'python2', 'python-setuptools', ]: ensure => installed}

  file {$seismon_dir: 
	ensure => directory, 
	owner => seismon,
	group => seismon,
	}

  file {"${seismon_dir}/install_seismon.sh": 
	ensure => present,
	source => 'puppet:///modules/seismon/seismon_dir/install_seismon.sh',
	owner => seismon,
	group => seismon,
	mode => '0755',
	}

  exec {"download_seismon_client":
	command => "/usr/bin/wget 'https://usgs.github.io/pdl/ProductClient.zip'",
        user => seismon,	
	cwd => $seismon_dir,
	require => File[$seismon_dir],
	creates => "${seismon_dir}/ProductClient.zip",
	}

  exec {"install client":
	user => seismon,
	command => "${seismon_dir}/install_seismon.sh",
	cwd => $seismon_dir,
	creates => "${seismon_dir}/client_installed.flag",
	require => [Exec['download_seismon_client'], File["${seismon_dir}/install_seismon.sh"], Package['unzip'], Package['default-jre']],
	}

# create systemd unit to run script
  file {'/etc/systemd/system/seismon_client.service':
	content => "[Unit]
Description= Seismon client

[Service]
ExecStart=/usr/bin/java -jar ProductClient.jar --receive  --configFile=config.ini
User=seismon
Type=simple
WorkingDirectory=${seismon_dir}/ProductClient
StandardError=null

[Install]
WantedBy=multi-user.target
",
	require => Exec['install client'],
	notify => Exec['seismon daemon reload'],
	}

  exec {'seismon daemon reload':
	command => '/usr/bin/systemctl daemon-reload',
	refreshonly => true,
	}

  service {'seismon_client':
	require => [File['/etc/systemd/system/seismon_client.service'], Exec['seismon daemon reload']],
	enable => true,
	ensure => running,
	}
	

  # install seismon source
  $seismon_src = "${seismon_dir}/seismon"

  exec {'clone_seismon_source':
	user => seismon,
	cwd => $seismon_dir,
	command => "/usr/bin/git clone -b ${repo_branch} ${repo_url}",
	creates => $seismon_src,
	require => [Package['git'], File[$seismon_dir]],
	}

  file {"${seismon_dir}/install_seismon_src.sh": 
	ensure => present,
	source => 'puppet:///modules/seismon/seismon_dir/install_seismon_src.sh',
	owner => seismon,
	group => seismon,
	mode => '0755',
	}

  exec {'install_seismon_source':
	user => seismon,
	cwd => $seismon_src,
	command => "${seismon_dir}/install_seismon_src.sh",
	require => [File["${seismon_dir}/install_seismon_src.sh"], Exec['clone_seismon_source']],
	creates => "${seismon_dir}/src_installed.flag",
	}

  ## update config.yaml
  # file {"${seismon_dir}/seismon/seismon/input/config.yaml":
	# content => epp("seismon/seismon/seismon/input/config.yaml.epp",
	# 	{seismon_dir => $seismon_dir, }),
	# owner => seismon,
	# group => seismon,
	# require => Exec['install_seismon_source'],
	# }

  # create a necessary directory
  file {"${seismon_src}/seismon/tests/new_events": 
	ensure => directory, 
	owner => seismon,
	group => seismon,
	}

  # generate ssh key
  exec {'generate seismon ssh key':
	user => seismon,
	cwd => $seismon_dir,
	command => "/usr/bin/ssh-keygen -q -f ${seismon_dir}/.ssh/id_rsa -N ''",
	creates => "${seismon_dir}/.ssh/id_rsa.pub",
	} 
	
  # database initialization - run only once - will timeout and fail
  file {"${seismon_dir}/initialize_database.sh":
	ensure => present,
	source => 'puppet:///modules/seismon/seismon_dir/initialize_database.sh',
	owner => seismon,
	group => seismon,
	mode => '0755',
	}

  exec {'initialize seismon database':
	user => seismon,
	refreshonly => true,
	cwd => "${seismon_src}/seismon",
	command => "${seismon_dir}/initialize_database.sh",
	subscribe => Exec['create_seismon_db'],
	require => [File["${seismon_dir}/initialize_database.sh"], 
		File["${seismon_src}/seismon/tests/new_events"],
		# File["${seismon_dir}/seismon/seismon/input/config.yaml"],
  ],
	timeout => 120,
	environment => [ "PYTHONPATH=${seismon_src}", ],
	}

  # create the systemd unit files
  file {'/etc/systemd/system/seismon.service':
	content => "[Unit]
Description= seismon models

[Service]
ExecStart=${seismon_dir}/start_models.sh
User=seismon
WorkingDirectory=${seismon_src}/seismon
Environment='PYTHONPATH=${seismon_src}'
Restart=on-failure

[Install]
WantedBy=multi-user.target
",
	notify => Exec['seismon daemon reload'],
	}

  file {'/etc/systemd/system/seismon-epics-ioc.service':
	content => "[Unit]
Description= seismon EPICS IOC

[Service]
ExecStart=${seismon_dir}/start_ioc.sh
User=seismon
Environment='SITE=$site' 'IFO=$ifo'

[Install]
WantedBy=multi-user.target
",
	notify => Exec['seismon daemon reload'],
	}

  file {'/etc/systemd/system/seismon-epics-client.service':
	content => "[Unit]
Description= seismon EPICS client 

[Service]
ExecStart=${seismon_dir}/start_epics_client.sh
User=seismon
Environment='SITE=$site' 'IFO=$ifo'
WorkingDirectory=${seismon_src}/seismon

[Install]
WantedBy=multi-user.target
",
	notify => Exec['seismon daemon reload'],
	}

	# create service run scripts
	file {"${seismon_dir}/start_models.sh":
		owner => seismon,
		group => seismon,
		mode => '0755',
		source => 'puppet:///modules/seismon/seismon_dir/start_models.sh',
		notify => Service['seismon'],
		}

	file {"${seismon_dir}/start_ioc.sh":
		owner => seismon,
		group => seismon,
		mode => '0755',
		source => 'puppet:///modules/seismon/seismon_dir/start_ioc.sh',
		notify => Service['seismon-epics-ioc'],
		}

	file {"${seismon_dir}/start_epics_client.sh":
		owner => seismon,
		group => seismon,
		mode => '0755',
		source => 'puppet:///modules/seismon/seismon_dir/start_epics_client.sh',
		notify => Service['seismon-epics-client'],
		}

	# setup services
	service{['seismon','seismon-epics-ioc', 'seismon-epics-client']:
		enable => true,
		ensure => running,
		require => [ Exec['seismon daemon reload'],
			File["${seismon_dir}/start_models.sh"],
			File["${seismon_dir}/start_ioc.sh"],
			File["${seismon_dir}/start_epics_client.sh"],
			Exec['initialize seismon database'], ],
		}
}

